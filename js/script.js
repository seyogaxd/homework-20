let currentIndex = 0;
      let intervalId;
      let timerElement = document.getElementById('timer');
      let timerValue = 3.0;
      let isSlideshowRunning = false;

      function showImage(index) {
        const images = document.querySelectorAll('.image-to-show');
        images.forEach((image, i) => {
          if (i === index) {
            image.style.display = 'block';
            image.style.animation = 'fadeIn 0.5s ease-in-out';
          } else {
            image.style.display = 'none';
          }
        });
      }

      function updateTimer() {
        timerElement.textContent = `Таймер: ${timerValue.toFixed(1)}`;
      }

      function startSlideshow() {
        if (!isSlideshowRunning) {
          isSlideshowRunning = true;
          intervalId = setInterval(() => {
            updateTimer();
            if (timerValue <= 0) {
              timerValue = 3.0;
              currentIndex = (currentIndex + 1) % 4;
              showImage(currentIndex);
            }
            timerValue -= 0.1;
          }, 100);
        }
      }

      function stopSlideshow() {
        clearInterval(intervalId);
        isSlideshowRunning = false;
      }

      function resumeSlideshow() {
        startSlideshow();
      }

      showImage(0);
      startSlideshow();